package src.view;

import java.awt.Point;
import java.awt.geom.Ellipse2D;

import src.model.GameConstant;
import src.utilities.BonusType;

/**
 * 
 * @author Federico Galdenzi
 * This class represent the bonus in the MatchZone.
 */
public class BonusView {
    private Ellipse2D bonus;
    private BonusType type;

    /**
     * This is the constructor of the BonusView class.
     * @param p the position of the graphic bonus
     */
    public BonusView(final Point p) {
        this.bonus = new Ellipse2D.Double();
        this.drawBonus(p.x, p.y, GameConstant.BONUS_DIAMETER);
    }

    /**
     * This is the constructor of the BonusView class.
     * @param p the position of the graphic bonus
     * @param bt the type of the bonus
     * 
     */
    public BonusView(final Point p, final BonusType bt) {
        this.bonus = new Ellipse2D.Double();
        this.type = bt;
        this.drawBonus(p.x, p.y, GameConstant.BONUS_DIAMETER);
    }
    private void drawBonus(final int bonusX, final int bonusY, final int radius) {
        this.bonus.setFrame(bonusX, bonusY, radius, radius);
    }

    /**
     * 
     * @return the bonus graphic instance
     */
    public final Ellipse2D getInstance() {
        return this.bonus;
    }

    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bonus == null) ? 0 : bonus.hashCode());
        return result;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof BonusView)) {
            return false;
        }
        BonusView other = (BonusView) obj;
        if (bonus == null) {
            if (other.bonus != null) {
                return false;
            }
        } else if (!bonus.equals(other.bonus)) {
            return false;
        }
        return true;
    }

    /**
     * @return the type of the bonus
     */
    public BonusType getType() {
        return this.type;
    }
}
