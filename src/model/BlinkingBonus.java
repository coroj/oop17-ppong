package src.model;

import src.observerpattern.ExpiringBonusTimerObserver;
/**
 * 
 * @author Federico Galdenzi
 * This class rappresent the Blinking bonus, it has a behaviour that reverse the visibility of the ball
 * at every interval.
 */
public class BlinkingBonus extends BonusTimer {
    /**
     * This is the constructor of the bonus timer which you have to pass the parameters.
     * @param ball the ball object
     * @param observer the ExpiringBonusTimerObserver object, in this case the BonusManager
     * @param interval the interval of the timer
     * @param duration the duration of the timer
     */
    public BlinkingBonus(final Ball ball, final ExpiringBonusTimerObserver observer, final int interval, final int duration) {
        super(ball, observer, interval, duration);
    }
    @Override
    public final void resetStrategy() {
        this.getBall().getProprierty().makeBallVisible();
    }

    @Override
    public final void applyStrategy() {
        this.start();
    }

    @Override
    public final void handleStrategy() {
        if (this.getBall().getProprierty().getVisibility()) {
            this.getBall().getProprierty().makeBallInvisible();
        } else {
            this.getBall().getProprierty().makeBallVisible();
        }
    }

}
