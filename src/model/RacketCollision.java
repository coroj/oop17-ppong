package src.model;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import src.utilities.Direction;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

/**
 * 
 * @author Federico Galdenzi
 *
 */
public class RacketCollision implements CollisionStrategy {
    private List<Racket> rackets;
    private Ball ball;
    private Optional<Point> collisionPoint;

    /**
     * this is the constructor of ShapeCollision.
     * 
     * @param ballModel
     *            the ball
     * @param racketOne
     *            the racketOne
     * @param racketTwo
     *            the racketTwo
     */
    public RacketCollision(final Ball ballModel, final Racket racketOne, final Racket racketTwo) {
        this.ball = ballModel;
        this.rackets = new ArrayList<>();
        this.rackets.add(racketOne);
        this.rackets.add(racketTwo);
    }

    private boolean check() {

        if (this.ball.getProprierty().getXDirection() == Direction.LEFT) {
            if (this.rackets.get(0).getBoundRacket().intersects(this.ball.getShape())) {
                this.ball.getProprierty().setX(this.rackets.get(0).getXDown());

                return true;
            }
        } else {
            if (this.rackets.get(1).getBoundRacket().intersects(this.ball.getShape())) {
                this.ball.getProprierty().setX(this.rackets.get(1).getXUp() - this.ball.getProprierty().getDiameter());

                return true;
            }
        }
        return false;
    }

    /**
     * the method used to do the bounce effect of the ball who collide with the
     * racket.
     * 
     * @param numberOfSpeedBonus
     *            the number of speedBonus applied to the ball.
     */
    public final void bounce(final int numberOfSpeedBonus) {
        int player;
        int firstSection;
        int constant;
        Random rnd = new Random();
        List<Rectangle> listSec = new ArrayList<>();
        player = (this.ball.getProprierty().getXDirection() == Direction.LEFT) ? 0 : 1;
        firstSection = this.rackets.get(player).getYUp();
        constant = this.rackets.get(player).getHeight() / GameConstant.NUMBER_OF_SECTION_BAR;

        for (int i = 0; i < GameConstant.NUMBER_OF_SECTION_BAR; i++) {
            listSec.add(new Rectangle(this.rackets.get(player).getXUp(), firstSection,
                    this.rackets.get(player).getWidth(), constant));
            firstSection = (int) (firstSection + constant);
        }
        for (int i = 0; i < GameConstant.NUMBER_OF_SECTION_BAR; i++) {
            if (listSec.get(i).intersects(this.ball.getShape())) {
                switch (i) {
                case 0:
                    //System.out.println("caso alto");
                    if (this.ball.getProprierty().getYDirection() != Direction.UP) {
                        this.ball.getProprierty().changeYDirection();
                    }
                    this.ball.getProprierty()
                            .setSpeedY(rnd.nextInt((GameConstant.MAX_ANGLE_BALL - GameConstant.MEDIUM_ANGLE_BALL) + 1)
                                    + GameConstant.MEDIUM_ANGLE_BALL);
                    this.ball.getProprierty().setSpeedX(
                            rnd.nextInt((GameConstant.BALL_MAX_SPEED_X - GameConstant.BALL_MEDIUM_SPEED_X) + 1)
                                    + GameConstant.BALL_MEDIUM_SPEED_X
                                    + (numberOfSpeedBonus * GameConstant.BONUS_SPEED_X));
                    break;
                case 1:
                    //System.out.println("caso centro");
                    this.ball.getProprierty().setSpeedY(0);
                    this.ball.getProprierty().setSpeedX(
                            GameConstant.BALL_MAX_SPEED_X + (numberOfSpeedBonus * GameConstant.BONUS_SPEED_X));
                    break;
                case 2:
                   // System.out.println("caso basso");
                    if (this.ball.getProprierty().getYDirection() != Direction.DOWN) {
                        this.ball.getProprierty().changeYDirection();
                    }
                    this.ball.getProprierty()
                            .setSpeedY(rnd.nextInt((GameConstant.MAX_ANGLE_BALL - GameConstant.MEDIUM_ANGLE_BALL) + 1)
                                    + GameConstant.MEDIUM_ANGLE_BALL);
                    this.ball.getProprierty().setSpeedX(
                            rnd.nextInt((GameConstant.BALL_MAX_SPEED_X - GameConstant.BALL_MEDIUM_SPEED_X) + 1)
                                    + GameConstant.BALL_MEDIUM_SPEED_X
                                    + (numberOfSpeedBonus * GameConstant.BONUS_SPEED_X));
                    break;
                 default:
                     break;
                }
            }
        }
        this.ball.getProprierty().changeXDirection();
    }

    @Override
    public final boolean hasCollision() {
        if (check()) {
            collisionPoint = Optional.of(new Point(this.ball.getProprierty().getX(), this.ball.getProprierty().getY()));
            return true;
        }
        collisionPoint = Optional.empty();
        return false;
    }

    @Override
    public final Optional<Point> getCollisionPoint() {
        return this.collisionPoint;
    }

    @Override
    public final boolean canBallMove() {
        return true;
    }
}
