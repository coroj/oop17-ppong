package src.model;

import java.awt.Point;

import src.utilities.BonusType; 
/**
 * 
 * @author Federico Galdenzi
 * This is the interface for BonusImpl class.
 */
public interface Bonus {
    /**
     * 
     * @return the position of the bonus.
     */
    Point getPosition();
    /**
     * 
     * @return the type of the bonus.
     */
    BonusType getType();
    /**
     * 
     * @return the diameter of the bonus.
     */
    int getDiameter();
}
