package src.model;

import java.awt.Point;
import java.util.Optional;
/**
 * 
 * @author Federico Galdenzi
 * The Collision interface provide methods that check if there is a Collision, 
 * get the collisionPosition and know if the ball can move after the HasCollision called.
 */
public interface CollisionStrategy {
    /**
     * This method check if there is a collision, when it will be override you have to implements the behavior you want to check.
     * @return a boolean true if there is a collision, false in there isn't.
     */
    boolean hasCollision();
    /**
     * This method return an optional that encapsulated a Point object who represent the Collision Point.
     * @return  if there is a collision return the Point if there isn't return Optional.empty()
     */
    Optional<Point> getCollisionPoint();
    /**
     * This method return if the ball can move.
     * It can't move if there is a collision in CollisionBorder class.
     * @return a boolean true if the ball can move, false if can't.
     */
    boolean canBallMove();
}
