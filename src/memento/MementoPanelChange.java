package src.memento;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * This class keeps in memory panel names, allowing to navigate among them.
 *
 */
public class MementoPanelChange {
    private Originator originator = new Originator();
    private CareTaker careTaker = new CareTaker();

    /**
     * 
     * @param name
     *            the name of the panel to be saved
     */
    public final void savePanelName(final String name) {
        originator.setState(name);
        careTaker.add(originator.saveStateToMemento());
    }

    /**
     * 
     * @return the previous saved panel name
     */
    public final String getPreviousPanelName() {
        String ret = null;
        if (!careTaker.mementoList.isEmpty()) {
            ret = new String(careTaker.mementoList.get(0).getState());
            careTaker.mementoList.remove(0);
        }
        return ret;
    }

    private class Memento {
        private String state;

        Memento(final String state) {
            this.state = state;
        }

        public String getState() {
            return state;
        }
    }

    private class Originator {
        private String state;

        public void setState(final String state) {
            this.state = state;
        }

        @SuppressWarnings("unused")
        public String getState() {
            return state;
        }

        public Memento saveStateToMemento() {
            return new Memento(state);
        }

        @SuppressWarnings("unused")
        public void getStateFromMemento(final Memento memento) {
            state = memento.getState();
        }
    }

    private class CareTaker {
        private List<Memento> mementoList = new ArrayList<Memento>();

        public void add(final Memento state) {
            mementoList.add(0, state);
        }

        @SuppressWarnings("unused")
        public Memento get(final int index) {
            return mementoList.get(index);
        }
    }
}
