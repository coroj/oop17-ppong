package src.testjunit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;

import org.junit.Before;
import org.junit.Test;

import src.model.Ball;
import src.model.BallImpl;
import src.model.BorderCollision;
import src.model.Boundary;
import src.model.CollisionStrategy;
import src.model.GameConstant;
import src.model.Racket;
import src.model.RacketImpl;
import src.model.RacketCollision;
import src.utilities.Direction;

/**
 * @author Jacopo Corina
 *
 */
public class TestJUnit {
    private int screenX = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
    private int screenY = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
    private int racketHeight = 100;
    private int racketWidth = 10;
    private int y1Before;
    private Boundary boundary;
    /**
     * Creates screen boundary and resets the previous player 1 state.
     */
    @Before
    public void before() {
        y1Before = 0;
        boundary = new Boundary(new Dimension(screenX, screenY));
    }

    /**
     * Tests racket movement.
     */
    @Test
    public void testRacketMovement() {
        // racket 1
        RacketImpl r1 = new RacketImpl(100, 100, racketWidth, racketHeight, boundary);
        y1Before = r1.getYDown();
        r1.move(Direction.DOWN);
        assertEquals(y1Before + r1.getHeight() / GameConstant.SLIDING_CONSTANT, r1.getYDown());
        y1Before = r1.getYUp();
        r1.move(Direction.UP);
        assertEquals(y1Before - r1.getHeight() / GameConstant.SLIDING_CONSTANT, r1.getYUp());
    }
    /**
     * Tests racket touching upper border behaviour.
     */
    @Test
    public void testRacketOnBorderUp() {
        RacketImpl r1 = new RacketImpl(0, 0, racketWidth, racketHeight, boundary);
        y1Before = r1.getYUp();
        r1.move(Direction.UP);
        assertEquals(y1Before, r1.getYUp());
    }
    /**
     * Tests racket touching lower border behaviour.
     */
    @Test
    public void testRacketOnBorderDown() {
        RacketImpl r1 = new RacketImpl(0, screenY - racketHeight, racketWidth, racketHeight, boundary);
        y1Before = r1.getYDown();
        r1.move(Direction.DOWN);
        assertEquals(y1Before, r1.getYDown());
    }

    /**
     * Tests goal made by player 2.
     */
    @Test
    public void testGoalRacket1() {
        BallImpl ball = new BallImpl(new Dimension(screenX, screenY), GameConstant.BALL_DIAMETER, GameConstant.BALL_MEDIUM_SPEED_X, GameConstant.BALL_MAX_SPEED_Y);
        if (ball.getProprierty().getXDirection() == Direction.RIGHT) {
            ball.getProprierty().changeXDirection();
        }
        while (!ball.hasScore()) {
            ball.move();
        }
        assertTrue(ball.getProprierty().getX() <= 0);

    }
    /**
     * Tests goal made by player 1.
     */
    @Test
    public void testGoalRacket2() {
        BallImpl ball = new BallImpl(new Dimension(screenX, screenY), GameConstant.BALL_DIAMETER,
                GameConstant.BALL_MEDIUM_SPEED_X, GameConstant.BALL_MAX_SPEED_Y);

        if (ball.getProprierty().getXDirection() == Direction.LEFT) {
            ball.getProprierty().changeXDirection();
        }
        while (!ball.hasScore()) {
            ball.move();
        }
        assertTrue(ball.getProprierty().getX() >= screenX);
    }
    /**
     * Test the collision between ball and player 1.
     */
    @Test
    public void testRacket1Collision() {

        Ball ball = new BallImpl(new Dimension(screenX, screenY), GameConstant.BALL_DIAMETER,
                GameConstant.BALL_MAX_SPEED_X, GameConstant.BALL_MIN_SPEED_Y);
        if (ball.getProprierty().getXDirection() == Direction.RIGHT) {
            ball.getProprierty().changeXDirection();
        }
        Racket racket = new RacketImpl(ball.getProprierty().getX() - 100, screenY / 2, racketWidth, racketHeight,
                boundary);
        CollisionStrategy collision = new RacketCollision(ball, racket, null);
        while (!racket.getBoundRacket().intersects(ball.getShape())) {
            assertFalse(collision.hasCollision());
            ball.move();
        }
        assertTrue(collision.hasCollision());
        ((RacketCollision) collision).bounce(0);
        assertTrue(ball.getProprierty().getXDirection() == Direction.RIGHT);

    }
    /**
     * Test the collision between ball and player 2.
     */
    @Test
    public void testRacket2Collision() {
        Ball ball = new BallImpl(new Dimension(screenX, screenY), GameConstant.BALL_DIAMETER,
                GameConstant.BALL_MAX_SPEED_X, GameConstant.BALL_MIN_SPEED_Y);
        if (ball.getProprierty().getXDirection() == Direction.LEFT) {
            ball.getProprierty().changeXDirection();
        }
        Racket racket = new RacketImpl(ball.getProprierty().getX() + 100, screenY / 2, racketWidth, racketHeight,
                boundary);
        CollisionStrategy collision = new RacketCollision(ball, null, racket);
        while (!racket.getBoundRacket().intersects(ball.getShape())) {
            assertFalse(collision.hasCollision());
            ball.move();
        }
        assertTrue(collision.hasCollision());
        ((RacketCollision) collision).bounce(0);
        assertTrue(ball.getProprierty().getXDirection() == Direction.LEFT);
    }
    /**
     * Tests the collision between the ball and the upper screen border.
     */
    @Test
    public void testUpperBorderCollision() {
        Ball ball = new BallImpl(new Dimension(screenX, screenY), GameConstant.BALL_DIAMETER, 0,
                GameConstant.BALL_MAX_SPEED_Y);
        if (ball.getProprierty().getYDirection() == Direction.DOWN) {
            ball.getProprierty().changeYDirection();
        }
        CollisionStrategy collision = new BorderCollision(ball);
        while (!ball.getShape()
                .intersects(new Rectangle(0, 0, ball.getProprierty().getBoundaryWindow().getXBoundLaneRight(), 1))) {
            ball.move();
        }
        assertTrue(collision.hasCollision());
        assertFalse(ball.getProprierty().getYDirection() == Direction.UP);
    }
    /**
     * Tests the collision between the ball and the lower screen border.
     */
    @Test
    public void testLowerBorderCollision() {
        Ball ball = new BallImpl(new Dimension(screenX, screenY), GameConstant.BALL_DIAMETER, 0,
                GameConstant.BALL_MAX_SPEED_Y);
        if (ball.getProprierty().getYDirection() == Direction.UP) {
            ball.getProprierty().changeYDirection();
        }
        CollisionStrategy collision = new BorderCollision(ball);
        while (!ball.getShape()
                .intersects(new Rectangle(0, ball.getProprierty().getBoundaryWindow().getYBoundLaneDown(),
                        ball.getProprierty().getBoundaryWindow().getXBoundLaneRight(), 1))) {
            ball.move();
        }
        assertTrue(collision.hasCollision());
        assertFalse(ball.getProprierty().getYDirection() == Direction.DOWN);
    }
}
