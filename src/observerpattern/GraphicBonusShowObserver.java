package src.observerpattern;

import java.awt.Point;

import src.model.Bonus;
import src.utilities.BonusType;
/**
 * 
 * @author Jacopo Corina
 * This interface could be used to manage the bonus graphics part appereance and proprierties when a bonus is applied or is expired.
 */
public interface GraphicBonusShowObserver {
    /**
     * 
     * @param bonus The point which indicates the bonus position
     */
    void removeBonus(Point bonus);
    /**
     * removes all the bonus graphics present.
     */
    void clearBonus();
    /**
     * 
     * @param bonus the bonus to add at the view
     */
    void addBonus(Bonus bonus);
    /**
     * 
     * @param state the ball visibility status
     */
    void applyBallVisibility(boolean state);
    /**
     * 
     * @param bt the bonus type to describe
     */
    void showHitBonusDescription(BonusType bt);
}
