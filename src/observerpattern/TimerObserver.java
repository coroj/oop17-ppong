package src.observerpattern;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface TimerObserver {
    /**
     * 
     * @param remainingTime the remaining match timer
     */
    void executeOnTimerUpdate(long remainingTime);
}
