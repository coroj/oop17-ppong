package src.controller;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.util.Optional;
import javax.swing.JButton;

import src.model.Ball;
import src.model.BallImpl;
import src.model.Boundary;
import src.model.GameConstant;
import src.model.GameStatus;
import src.model.GameStatusImpl;
import src.model.MatchTimer;
import src.model.OptionsValues;
import src.model.Racket;
import src.model.RacketImpl;
import src.observerpattern.ExpiringMatchTimerObserver;
import src.observerpattern.ScoreUpdateObserver;
import src.observerpattern.TimerObserver;
import src.resources.ResourcesManagement;
import src.utilities.Utility;
import src.view.ViewManager;
import src.view.OptionsView.COLORSTRINGS;
/**
 * This class represents the game controller.
 * @author Jacopo Corina
 * 
 */
public class GameController implements TimerObserver, ExpiringMatchTimerObserver, ScoreUpdateObserver {
    private ViewManager theView;
    private GameStatus theModel;
    private Ball ballModel;
    private BallAgentImpl ball;
    private RacketsMovement move;
    private MyKeyEvent event;
    private Racket player1;
    private Racket player2;
    private OptionsValues options;
    private OptionsManager optManager;
    private int result;
    private Boundary boundary;
    private PauseManager pm;
    private Optional<IARacketAgentImpl> rAgent;
    private Thread ballThread;
    private Thread racketsThread;
    private Thread comRacketAgent;
    /**
     * it is the constructor method of GameController.
     * @param theView the view manager
     */
    public GameController(final ViewManager theView) {
        this.theView = theView;
        this.options = new OptionsValues();
        this.optManager = new OptionsManagerImpl();
        this.theModel = new GameStatusImpl(options.getTimerSeconds(), options.getMaxScore());
        theModel.passObserverToTimer(GameController.this);
        theModel.passObserverToExpiringTimer(GameController.this);
        theView.getMatchZone().setOptionsValues(options);
        pm = new PauseManagerImpl();
        this.event = new MyKeyEvent(this);
        this.theView.getMatchZone().addMovePlayerListener(event);
        theView.getStartView().addPlayButtonListener(e -> {
            ResourcesManagement.playSound(ResourcesManagement.getMenuButtonSoundStream());
            this.boundary = new Boundary(new Dimension(theView.getMatchZone().getWidth(), theView.getMatchZone().getHeight()));
            this.player1 = new RacketImpl(GameConstant.RACKET_ONE_POSITION_X, GameConstant.RACKET_POSITION_Y, GameConstant.RACKET_WIDTH, GameConstant.RACKET_HEIGHT, this.boundary);
            this.player2 = new RacketImpl(GameConstant.RACKET_TWO_POSITION_X, GameConstant.RACKET_POSITION_Y, GameConstant.RACKET_WIDTH, GameConstant.RACKET_HEIGHT, this.boundary);
            this.ballModel = new BallImpl(new Dimension(this.theView.getMatchZone().getWidth(), this.theView.getMatchZone().getHeight()), GameConstant.BALL_DIAMETER);
            ball = new BallAgentImpl(ballModel, theView.getMatchZone().getBall(), player1, player2, this, theView.getMatchZone());
            result = theView.getMatchZone().showGameModeBox();
            if (result != GameConstant.VALUE_NOT_SET) {
                theModel.setPlayer1Name(theView.getMatchZone().showPlayerNameBox(1));
                if (result == 0) {
                    theModel.setPlayer2Name(theView.getMatchZone().showPlayerNameBox(2));
                    this.rAgent = Optional.empty();
                    this.move = new RacketsMovementImpl(player1, player2, theView);
                } else {
                    result = theView.getMatchZone().showDifficultyBox(); 
                    if (result != GameConstant.VALUE_NOT_SET) {
                        theModel.setPlayer2Name(GameConstant.DEFAULT_P2_NAME);
                        this.move = new RacketsMovementImpl(player1, theView);
                        if (result == 0) {
                            this.rAgent = Optional.of(new IARacketAgentImpl(this.ballModel, this.player2, this.theView, GameConstant.WAITING_TIME_IA_NORMAL));
                        } else {
                            this.rAgent = Optional.of(new IARacketAgentImpl(this.ballModel, this.player2, this.theView, GameConstant.WAITING_TIME_IA_HARD));
                        }
                    }
                }
                theModel.resetScore();
                goToPanel(theView.getStartView().getName(), theView.getGameView().getName());
                theView.getMatchZone().initializeMatchGraphics();
                theView.getMatchZone().setScore(theModel.getScore1(), theModel.getScore2());
                theModel.startMatch(); /*timer and score*/
                this.ballThread = new Thread(ball);
                this.ballThread.start();
                this.racketsThread = new Thread(move);
                this.racketsThread.start();
                this.rAgent.ifPresent(x -> {
                    this.comRacketAgent = new Thread(x);
                    this.comRacketAgent.start();
                });
            }
        });
        theView.getStartView().addStatisticsButtonListener(e -> {
            ResourcesManagement.playSound(ResourcesManagement.getMenuButtonSoundStream());
            theView.getStatisticsView().setColumnsName();
            theView.getStatisticsView().fillTableModel();
            theView.getStatisticsView().applyFilling();
            goToPanel(theView.getStartView().getName(), theView.getStatisticsView().getName());
        });
        theView.getStartView().addOptionsButtonListener(e -> {
            ResourcesManagement.playSound(ResourcesManagement.getMenuButtonSoundStream());
            goToPanel(theView.getStartView().getName(), theView.getOptionsView().getName());
        });
        theView.getOptionsView().addColorConfirmListener(e -> {
            optManager.applyColorOptions();
        });
        theView.getOptionsView().addSoundStateChangeListener(e -> {
            optManager.applySoundOptions();
        });
        theView.getOptionsView().addMaxScoreSwitchChangeListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                theView.getOptionsView().enableScoreSpinner();
                theView.getOptionsView().disableTimeSpinner();
                optManager.applyMatchOptions(theView.getOptionsView().getMaxScoreValue(), MatchTimer.DURATION_INFINITY);
            }
        });
        theView.getOptionsView().addMaxScoreValueChangeListener(e -> {
            optManager.applyMatchOptions(theView.getOptionsView().getMaxScoreValue(), MatchTimer.DURATION_INFINITY);
        });
        theView.getOptionsView().addTimerSwitchChangeListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                theView.getOptionsView().enableTimeSpinner();
                theView.getOptionsView().disableScoreSpinner();
                optManager.applyMatchOptions(GameConstant.VALUE_NOT_SET, theView.getOptionsView().getTimerValue());
            }
        });
        theView.getOptionsView().addTimerValueChangeListener(e -> {
            optManager.applyMatchOptions(GameConstant.VALUE_NOT_SET, theView.getOptionsView().getTimerValue());
        });




    }
    /**
     * @param oldPanel old panel name to save.
     * @param newPanel next panel to show
     */
    public void goToPanel(final String oldPanel, final String newPanel) {
        CardLayout cl = (CardLayout) (theView.getMainPane().getLayout());
        theModel.savePreviousPanelName(oldPanel);
        checkBarAndUpdateIt(newPanel);
        cl.show(theView.getMainPane(), newPanel);
    }
    /**
     * @param newPanel next panel to show
     */
    public void goToPanel(final String newPanel) {
        CardLayout cl = (CardLayout) (theView.getMainPane().getLayout());
        checkBarAndUpdateIt(newPanel);
        cl.show(theView.getMainPane(), newPanel);
    }
    private void checkBarAndUpdateIt(final String newPanel) {
        if (newPanel.equals(theView.getStartView().getName())
                || newPanel.equals(theView.getGameView().getName())
                || newPanel.equals(theView.getPauseView().getName())) {
            theView.getMenuBar().setVisible(false);
        } else {
            JButton b = (JButton) theView.getMenuBar().getComponent(0);
            for (ActionListener al : b.getActionListeners()) {
                b.removeActionListener(al);
            }
            b.addActionListener(e -> {
                String s = theModel.getPreviousPanelName();
                if (newPanel.equals(theView.getStatisticsView().getName())) {
                    theView.getStatisticsView().removeTable();
                }
                ResourcesManagement.playSound(ResourcesManagement.getMenuButtonSoundStream());
                this.goToPanel(s);
            });
            theView.getMenuBar().setVisible(true);
        }
    }
    /**
     * this method pauses the game. 
     * @param key is the key pressed
     */
    public final void pauseGame(final int key) {
        if (key == KeyEvent.VK_ESCAPE) {
            pm.invokePauseMenu();
        }
    }
    /**
     * set upPlayer1 true or false.
     * @param b  true or false value
     */
    public final void setUpPlayer1(final boolean b) {
        this.move.setUpPlayer1(b);
    }
    /**
     * set downPlayer1 true or false.
     * @param b true or false value
     */
    public final void setDownPlayer1(final boolean b) {
        this.move.setDownPlayer1(b);
    }
    /**
     * set upPlayer2 true or false.
     * @param b true or false value
     */
    public final void setUpPlayer2(final boolean b) {
        this.move.setUpPlayer2(b);
    }
    /**
     * set downPlayer2 true or false.
     * @param b true or false value
     */
    public final void setDownPlayer2(final boolean b) {
        this.move.setDownPlayer2(b);
    }

    /**
     * 
     * @author Jacopo Corina
     *
     */
    public final class PauseManagerImpl implements PauseManager {
        /**
         * 
         */
        public PauseManagerImpl() {
            theView.getPauseView().addQuitButtonListener(e -> {
                ResourcesManagement.playSound(ResourcesManagement.getMenuButtonSoundStream());
                quit();
            });
            theView.getPauseView().addResumeButtonListener(e -> {
                ResourcesManagement.playSound(ResourcesManagement.getMenuButtonSoundStream());
                resume();
            });
        }
        /**
         * Quit the match.
         */
        @Override
        public void quit() {
            theModel.resetMatchTimer(); 
            ball.stopThread();
            move.stopThread();
            rAgent.ifPresent(x -> x.stopThread());
            goToPanel(theView.getStartView().getName());
        }
        /**
         * Pause the match.
         */
        @Override
        public void pause() {
            theModel.pauseMatchTimer(); 
            ball.pauseThread();
            move.pauseThread();
            rAgent.ifPresent(x -> x.pauseThread());
            goToPanel(theView.getPauseView().getName());
        }
        /**
         * Resume the match.
         */
        @Override
        public void resume() {
            theModel.resumeMatchTimer(); 
            ball.resumeThread();
            move.resumeThread();
            rAgent.ifPresent(x -> x.resumeThread());
            goToPanel(theView.getGameView().getName());
        }
        /**
         * Invoke the pause menu.
         */
        public void invokePauseMenu() {
            if (!ball.hasStopped()) {
                pause();
            }
        }
    }
    /**
     * 
     * @author Jacopo Corina
     *
     */
    public class OptionsManagerImpl implements OptionsManager {
        /**
         * Applies the color options to the graphics.
         */
        @Override
        public void applyColorOptions() {
            COLORSTRINGS s = theView.getOptionsView().changingElement();
            Color c = theView.getOptionsView().getChoosenColor();
            if (COLORSTRINGS.BallColor.equals(s)) {
                options.setBallColor(c);
            } else if (COLORSTRINGS.Player1Color.equals(s)) {
                options.setRacket1Color(c);
            } else if (COLORSTRINGS.Player2Color.equals(s)) {
                options.setRacket2Color(c);
            } else if (COLORSTRINGS.BackgroundColor.equals(s)) {
                options.setBackgroundColor(c);
            }
            theView.getMatchZone().setOptionsValues(options);
        }
        /**
         * Applies the sound options to the game.
         */
        @Override
        public void applySoundOptions() {
            options.setSoundEnabled(theView.getOptionsView().isSoundEnabled());
            ResourcesManagement.switchSound();
        }
        /**@param score The maximum score
         * @param time The maximum time
         * Applies the match options to the game.
         */
        @Override
        public void applyMatchOptions(final int score, final int time) {
            options.setMaxScore(score);
            options.setTimerSeconds(time);
            theModel.setMaximumScore(score);
            if (score == GameConstant.VALUE_NOT_SET) {
                theModel.passObserverToTimer(GameController.this);
                theModel.passObserverToExpiringTimer(GameController.this);
            } else {
                theModel.passObserverToTimer(null);
                theModel.passObserverToExpiringTimer(null);
            }
            theModel.setTimerValue(time);
            theView.getMatchZone().setOptionsValues(options);



        }
    }
    @Override
    public final void executeOnScoreUpdate(final int playerNumber) {
        int winner = GameConstant.VALUE_NOT_SET;
        if (playerNumber == 1) {
            theModel.addScorePlayer1();
        } else if (playerNumber == 2) {
            theModel.addScorePlayer2();
        }
        theView.getMatchZone().setScore(theModel.getScore1(), theModel.getScore2());
        winner = theModel.getWinnerScoreMode();
        if ((theModel.getWinnerScoreMode()) >= 0) {
            gameLoopEnd(winner);
        }
    }
    @Override
    public final void executeOnTimerUpdate(final long remainingTime) {
        theView.getMatchZone().showRemainingTime((int) (remainingTime / Utility.MSEC_CONST));
    }
    @Override
    public final void executeOnMatchTimerExpiration() {
        gameLoopEnd(theModel.getWinnerTimerMode());
    }
    private void gameLoopEnd(final int winner) {
        pm.quit();
        theView.getMatchZone().showWinner(winner);
        ResourcesManagement.saveScore(theModel.getScore());
    }
}
