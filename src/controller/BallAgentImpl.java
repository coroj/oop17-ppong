package src.controller;

import java.util.ArrayList;

import java.util.List;

import src.model.Ball;
import src.model.BonusCollision;
import src.model.BonusManager;
import src.model.BorderCollision;
import src.model.CollisionStrategy;
import src.model.GameConstant;
import src.model.Racket;
import src.model.RacketCollision;
import src.model.SpeedBonus;
import src.observerpattern.GraphicBonusShowObserver;
import src.observerpattern.ObservableGraphicBonusShow;
import src.observerpattern.ObservableScoreUpdate;
import src.observerpattern.ScoreUpdateObserver;
import src.resources.ResourcesManagement;
import src.utilities.BonusType;
import src.view.BallView;

/**
 * 
 * @author Federico Galdenzi
 *
 */
public class BallAgentImpl implements BallAgent, ObservableScoreUpdate, ObservableGraphicBonusShow, ThreadManagement {
    private boolean stopped;
    private Ball ballModel;
    private BallView ballView;
    private ScoreUpdateObserver obs;
    private List<CollisionStrategy> collisionList;
    private BonusManager bm;
    private GraphicBonusShowObserver graphicObs;
    private boolean suspended;
    private static final int BORDER_COLLISION = 0;
    private static final int RACKET_COLLISION = 1;
    private static final int BONUS_COLLISION = 2;

    /**
     * 
     * @param ballModel
     *            the ball model
     * @param ballView
     *            the ball graphics
     * @param racketOne
     *            the player 1 racket model
     * @param racketTwo
     *            the player 2 racket model
     * @param obs
     *            the score update observer
     * @param obs2
     *            the graphics bonus observer
     */
    public BallAgentImpl(final Ball ballModel, final BallView ballView, final Racket racketOne, final Racket racketTwo,
            final ScoreUpdateObserver obs, final GraphicBonusShowObserver obs2) {
        this.suspended = false;
        this.stopped = false;
        this.ballModel = ballModel;
        this.ballView = ballView;
        this.obs = obs;
        this.graphicObs = obs2;

        this.bm = new BonusManager(GameConstant.MAX_BONUS, this.ballModel.getProprierty().getBoundaryWindow(), graphicObs);
        this.bm.generateExpectedMaximumBonus();
        this.bm.getGeneratedBonusList().forEach(x -> this.graphicObs.addBonus(x));
        this.collisionList = new ArrayList<>();
        this.collisionList.add(new BorderCollision(ballModel));
        this.collisionList.add(new RacketCollision(ballModel, racketOne, racketTwo));
        this.collisionList.add(new BonusCollision(ballModel, bm.getGeneratedBonusList()));

    }

    @Override
    public final void pauseThread() {
        suspended = true;
        this.bm.pauseAppliedBonus();
        this.bm.pauseBonusGeneratorTimers();
    }

    @Override
    public final synchronized void resumeThread() {
        suspended = false;
        this.bm.resumeAppliedBonus();
        this.bm.resumeBonusGeneratorTimers();
        notify();
    }

    @Override
    public final synchronized void stopThread() {
        stopped = true;
        this.bm.stopBonusGeneratorTimers();
        this.bm.deleteAllAppliedBonus();
        this.graphicObs.clearBonus();
        notify();
    }

    @Override
    public final void run() {
        while (!hasStopped()) {

            if (!ballModel.hasScore()) {
                try {
                    collisionList.forEach(c -> c.hasCollision());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                if (collisionList.stream().allMatch(c -> c.canBallMove())) {
                    this.ballModel.move();
                }

                if (collisionList.get(BORDER_COLLISION).getCollisionPoint().isPresent()) {
                    ResourcesManagement.playSound(ResourcesManagement.getBorderHitSoundStream());
                }
                if (collisionList.get(RACKET_COLLISION).getCollisionPoint().isPresent()) {
                    ResourcesManagement.playSound(ResourcesManagement.getRacketHitSoundStream());
                    ((RacketCollision) this.collisionList.get(RACKET_COLLISION)).bounce((int) this.bm.getAppliedBonus().stream()
                            .filter(x -> x.getClass().isAssignableFrom(SpeedBonus.class)).count());
                }
                if (collisionList.get(2).getCollisionPoint().isPresent()) {
                    ResourcesManagement.playSound(ResourcesManagement.getBonusHitSoundStream());
                    graphicObs.showHitBonusDescription(((BonusCollision) collisionList.get(BONUS_COLLISION)).getHitBonusType().get());
                    graphicObs.removeBonus(collisionList.get(2).getCollisionPoint().get());
                    this.bm.addAppliedBonus(this.ballModel,
                            ((BonusCollision) collisionList.get(BONUS_COLLISION)).getHitBonus().get());

                }
                graphicObs.applyBallVisibility(this.ballModel.getProprierty().getVisibility());
                this.ballView.drawBall(ballModel.getProprierty().getX(), ballModel.getProprierty().getY(),
                        ballModel.getProprierty().getDiameter());

            } else {
                ResourcesManagement.playSound(ResourcesManagement.getPlayerGoalSoundStream());
                this.bm.deleteAllAppliedBonus();
                this.graphicObs.showHitBonusDescription(BonusType.NONE);
                notifyScoreUpdateObserver(ballModel.getNewPointTo());
                ballModel.resetHasScore();
                ballModel.getProprierty().changeXDirection();
                ballModel.getProprierty().restoreOriginalPosition();
                ballModel.getProprierty().randomizeSpeed();
            }
            try {
                Thread.sleep(GameConstant.GAME_LOOP_SLEEP_TIME);
                synchronized (this) {
                    if (suspended) {
                        wait();
                    }
                }
            } catch (InterruptedException ex) {
            }
        }
    }

    @Override
    public final void setScoreUpdateObserver(final ScoreUpdateObserver s) {
        this.obs = s;
    }

    @Override
    public final void notifyScoreUpdateObserver(final int playerNumber) {
        obs.executeOnScoreUpdate(playerNumber);
    }

    @Override
    public final void setGraphicBonusShowObserver(final GraphicBonusShowObserver obs) {
        this.graphicObs = obs;
    }

    @Override
    public final boolean hasStopped() {
        return stopped;
    }

}
