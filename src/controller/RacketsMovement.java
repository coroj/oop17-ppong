package src.controller;
/**
 * 
 * @author Luca Valentini
 *
 */
public interface RacketsMovement extends ThreadManagement, Runnable {
    /**
     * this is a set method to upPlayer1.
     * @param upPlayer1 the upPlayer1 to set
     */
    void setUpPlayer1(boolean upPlayer1);
    /**
     * this is a set method to downPlayer1.
     * @param downPlayer1 the downPlayer1 to set
     */
    void setDownPlayer1(boolean downPlayer1);
    /**
     * this is a set method to downPlayer2.
     * @param downPlayer2 the downPlayer2 to set
     */
    void setDownPlayer2(boolean downPlayer2);
    /**
     * this is a set method to upPlayer2.
     * @param upPlayer2 the upPlayer2 to set
     */
    void setUpPlayer2(boolean upPlayer2);
 
}
