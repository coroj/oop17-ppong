package src.controller;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface OptionsManager {
    /**
     * 
     */
    void applyColorOptions();
    /**
     * @param score maximum score
     * @param time  maximum time
     */
    void applyMatchOptions(int score, int time);
    /**
     * 
     */
    void applySoundOptions();
}
